﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_design_patern_csharp.CompositeDesign.Component
{
    interface IMusic
    {
        void getDetails(int indentation);
    }
}
