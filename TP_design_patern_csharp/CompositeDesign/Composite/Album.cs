﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP_design_patern_csharp.CompositeDesign.Component;
using TP_design_patern_csharp.Composite.Leaf;

namespace TP_design_patern_csharp.CompositeDesign.Composite
{
    class Album : IMusic
    {
        public string Name { get; set; }
        public string Band { get; set; }
        public List<IMusic> Tracks { get; set; }

        public Album(string name, string band)
        {
            Name = name;
            Band = band;
            Tracks = new List<IMusic>();
        }
        public void getDetails(int indentation)
        {
            Console.WriteLine(string.Format("Nom : {0} , Groupe : {1} ",
                new String('-', indentation), Name, Band));
            foreach (var song in Tracks)
            {
                song.getDetails(indentation + 1);
            }
        }
    }
}
