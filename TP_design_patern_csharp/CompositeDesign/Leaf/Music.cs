﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP_design_patern_csharp.CompositeDesign.Component;

namespace TP_design_patern_csharp.Composite.Leaf
{
    class Music : IMusic
    {
        public string Name { get; set; }
        public string Band { get; set; }

        public Music(string name, string band)
        {
            Name = name;
            Band = band;
        }
        public void getDetails(int indentation)
        {           
            Console.WriteLine(string.Format("Nom : {0} , Groupe : {1} (Leaf)", 
            new String('-', indentation), Name, Band));
        }
    }
}
