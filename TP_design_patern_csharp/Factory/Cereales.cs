﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_design_patern_csharp.Factory
{
    internal class Cereales : IArticles
    {
        public string _title { get; set; }
        public string _type { get; set; }
        public int _quantity { get; set; }
        public float _price { get; set; }

        public Cereales(string title, int quantity, float price)
        {
            _title = title;
            _type = "Céreales";
            _quantity = quantity;
            _price = price;
        }
    }
}
