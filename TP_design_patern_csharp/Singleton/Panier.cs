﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP_design_patern_csharp.Factory;

namespace TP_design_patern_csharp.Singleton
{
    internal class Panier
    {
        private List<IArticles> _article { get; set; }

        private Panier(List<IArticles> articles)
        {
            _article = articles;
        }

        private static Panier Instance;

        public static Panier GetInstance()
        {
            if (Instance == null)
            {
                Instance = new Panier(new List<IArticles>());
                Console.WriteLine("Singleton créer");
            }
            else
            {
                Console.WriteLine("Singleton existe déjà");
            }
            return Instance;
        }

        public void PrintCourse()
        {
            foreach (IArticles art in _article)
            {
                Console.WriteLine(art._quantity + " " + art._title + ", " + art._type + ", " + art._price.ToString() + " total = " + art.getTotal().ToString());
            }
            Console.WriteLine("Le prix total des achats est " + getPanierTotal(_article));
        }

        public void addArticles(IArticles articles)
        {
            _article.Add(articles);
        }
        internal float getPanierTotal(List<IArticles> articles)
        {
            float total = 0;
            foreach (var item in articles)
            {
                total += item.getTotal();
            }
            return total;
        }

        public float getFinal()
        {
            return getPanierTotal(_article);
        }
    }
}
