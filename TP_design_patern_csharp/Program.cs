﻿using TP_design_patern_csharp;
using TP_design_patern_csharp.Factory;
using TP_design_patern_csharp.Singleton;
using TP_design_patern_csharp.CompositeDesign.Component;
using TP_design_patern_csharp.CompositeDesign.Composite;
using TP_design_patern_csharp.Composite.Leaf;
using TP_design_patern_csharp.ChaineOfResponsability;

class Program
{
    static void Main(string[] args)
    {
        /*
         * Bonjour, pour ce tp j'ai créer une petite application boutique virtuel
         * j'ai utilisé pour commencer le design pattern Singleton pour créer une seul instance du pannier pour effectuer les courses
         * je n'ai pas sécurisé le readline par manque de temps je vous prie de bien vouloir jouer le jeu de bien entrer des nombres entier.
         * par la suite j'ai creer une factory pour la création d'articles qui se charge de la gestions de ceux ci ainsi que leur differences.
         * J'ai également fait usage d'un coposite pour ajouté un peu de musique imaginaire au tout
         * et ensuite j'ai utilise le pattern de chaine de responçabilité pour voir si le pannier ne question mérite une promo ( un pannier dépançant les 15€ vous indiquera la promotion optenue).
         * J'ai essayer de détailler dans ce commantaire ce que j'ai fais.
         */

        Panier p1 = Panier.GetInstance();
        Console.WriteLine("Combien voulez vous de Pomme ?");
        int tmp = Int32.Parse(Console.ReadLine());

        IArticles a = new Fruit ("pomme", tmp, (float)1.54);

        Console.WriteLine("Combien voulez vous de Poisson ?");
        tmp = Int32.Parse(Console.ReadLine());

        IArticles b = new Viande ("Poisson", tmp, (float)0.99);

        Console.WriteLine("Combien voulez vous de Carrotte ?");
        tmp = Int32.Parse(Console.ReadLine());

        IArticles c = new Legumes("Carrote", tmp, (float)2.95);

        Console.WriteLine("Combien voulez vous de Riz ?");
        tmp = Int32.Parse(Console.ReadLine());

        IArticles d = new Cereales("riz", tmp, (float)1.05);


        p1.addArticles(a);
        p1.addArticles(b);
        p1.addArticles(c);
        p1.addArticles(d);

               
        
        p1.PrintCourse();


        Request request = new Request() { Data = p1 };
        Promo promo = new Promo();

        promo.Process(request);

        foreach(string msg in request.MessageValidation )
        {
            Console.WriteLine(msg);
        }

        IMusic aSong = new Music("Thriller", "Michael Jackson");
        IMusic bSong = new Music("Beat It", "Michael Jackson");
        IMusic cSong = new Music("Billie Jean", "Michael Jackson");
        IMusic dSong = new Music("I'll Be Missing You", "Puff Daddy");

        IMusic aAlbum = new Album("Thriller", "Michael Jackson") { Tracks = {aSong, bSong, cSong}};
        IMusic bAlbum = new Album("No Way Out", "Puff Daddy") { Tracks = { dSong} };

        aAlbum.getDetails(1);
        bAlbum.getDetails(1);
    }
}