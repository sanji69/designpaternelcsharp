﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_design_patern_csharp.ChaineOfResponsability
{
    class Promo : BaseRequest
    {
        public override void Process(Request request)
        {
            if (request.Data is Singleton.Panier panier )
            {

                if (panier.getFinal() > 20) request.MessageValidation.Add("Promotion 5€ de bon d'achat");
                if(_nextGestionnaire != null) _nextGestionnaire.Process(request);
            }
            else
            {
                throw new Exception("Donnée invalide");
            }
        }
    }
}
