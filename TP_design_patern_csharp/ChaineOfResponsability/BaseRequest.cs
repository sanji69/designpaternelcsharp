﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_design_patern_csharp.ChaineOfResponsability
{
    internal class BaseRequest : IRequest
    {
        protected IRequest _nextGestionnaire { get; set; }

        public BaseRequest()
        {
            _nextGestionnaire = null;
        }

        //Méthode déclaré virtuel car elle ne sera jamais appelé mais héritant de l'interface de gestion elle dois tout de même être implémenté
        public virtual void Process(Request request)
        {
            throw new NotImplementedException();
        }

        public void setNextGestionnaire(IRequest next)
        {
            _nextGestionnaire = next;
        }
    }
}
