﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_design_patern_csharp.ChaineOfResponsability
{
    class Request
    {
        public object Data { get; set; }
        public List<string> MessageValidation;

        public Request()
        {
            MessageValidation = new List<string>();
        }
    }
}
