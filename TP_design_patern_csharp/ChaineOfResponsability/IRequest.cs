﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_design_patern_csharp.ChaineOfResponsability
{
    internal interface IRequest
    {
        void setNextGestionnaire(IRequest next);
        void Process(Request request);
    }
}
